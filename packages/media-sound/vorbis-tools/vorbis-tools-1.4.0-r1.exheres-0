# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Tools for the Ogg Vorbis audio format"
DESCRIPTION="
vorbis-tools contains oggenc (an encoder), ogg123 (a playback tool), ogginfo
(displays ogg information), oggdec (decodes ogg files), vcut (ogg file
splitter), and vorbiscomment (ogg comment editor).
"
HOMEPAGE="http://www.vorbis.com/"
DOWNLOADS="https://downloads.xiph.org/releases/vorbis/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    flac
    ogg123 [[ description = [ Ogg Vorbis audio player ] ]]
    speex  [[
        description = [ Add support for the speex audio codec in the Ogg123 audio player ]
        requires = [ ogg123 ]
    ]]
"

# TODO: support for media-libs/libkate::media-unofficial
# oggenc needs either kate or flac
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libogg[>=1.0]
        media-libs/libvorbis[>=1.3.0]
        flac? ( media-libs/flac[>=1.1.3] )
        ogg123? (
            media-libs/libao[>=1.0.0]
            net-misc/curl
        )
        speex? ( media-libs/speex )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.4.0-CVE-2014-9638.patch
    "${FILES}"/${PN}-1.4.0-CVE-2014-9640.patch
    "${FILES}"/Always-link-to-lm-for-oggenc-and-ogg123.patch
)

AT_M4DIR=( m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-{ogg{dec,info},vcut,vorbiscomment}
    --disable-static
    --without-kate
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "flac oggenc"
    ogg123
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    flac
    speex
)

