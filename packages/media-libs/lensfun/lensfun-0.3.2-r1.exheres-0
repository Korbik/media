# Copyright 2010 Timothy Redaelli <timothy@redaelli.eu>
# Copyright 2012-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ] \
    cmake \
    python [ blacklist=2 multibuild=false ]

SUMMARY="A library for rectifying and simulating photographic lens distortions"

LICENCES="LGPL-3 CCPL-Attribution-ShareAlike-3.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    doc
    platform: amd64
    x86_cpu_features: sse sse2
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            dev-python/docutils [[ note = [ rst2man ] ]]
        )
    build+run:
        dev-libs/glib:2[>=2.40]
        media-libs/libpng:=
        sys-libs/zlib
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/9ec857bb403accc262a9b5a9c2921b5c064fc9c8.patch
)

src_prepare() {
    cmake_src_prepare

    # fix shebangs
    edo sed \
        -e "s:#!/usr/bin/env python3:#!/usr/bin/env python$(python_get_abi):g" \
        -i apps/lensfun-{add-adapter,update-data}
}

src_configure() {
    local cmakeargs=(
        -DBUILD_AUXFUN:BOOL=TRUE
        -DBUILD_LENSTOOL:BOOL=TRUE
        -DBUILD_STATIC:BOOL=FALSE
        # Tests are expected to be installed
        -DBUILD_TESTS:BOOL=FALSE
        -DINSTALL_HELPER_SCRIPTS:BOOL=TRUE
        -DPYTHON:PATH=${PYTHON}
        $(cmake_build DOC)
    )

    if option platform:amd64 || option x86_cpu_features:sse ; then
        cmakeargs+=( -DBUILD_FOR_SSE:BOOL=TRUE )
    fi

    if option platform:amd64 || option x86_cpu_features:sse2 ; then
        cmakeargs+=( -DBUILD_FOR_SSE2:BOOL=TRUE )
    fi

    ecmake "${cmakeargs[@]}"
}

src_install() {
    cmake_src_install

    # used by the lensfun-update-data tool
    keepdir /var/lib/lensfun-updates
}

