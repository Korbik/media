From 250c59f783f11e5b23eea4cb73063b6ecb28453e Mon Sep 17 00:00:00 2001
From: Tsuda Kageyu <tsuda.kageyu@gmail.com>
Date: Fri, 9 Dec 2016 09:42:29 +0900
Subject: [PATCH] Remove optional dependencies on Boost's dynamic libraries.

Using precompiled Boost libraries can lead to depending on external dynamic libraries.
---
 ConfigureChecks.cmake          | 75 +++++++++++++++---------------------------
 config.h.cmake                 |  2 --
 taglib/CMakeLists.txt          | 12 ++-----
 taglib/toolkit/trefcounter.cpp |  5 ---
 taglib/toolkit/tzlib.cpp       | 46 +++-----------------------
 5 files changed, 33 insertions(+), 107 deletions(-)

diff --git a/ConfigureChecks.cmake b/ConfigureChecks.cmake
index fe36571..253af33 100644
--- a/ConfigureChecks.cmake
+++ b/ConfigureChecks.cmake
@@ -54,61 +54,47 @@ check_cxx_source_compiles("
 " HAVE_STD_ATOMIC)
 
 if(NOT HAVE_STD_ATOMIC)
-  
-  # We will not find BOOST_ATOMIC on macOS when BUILD_FRAMEWORK is set, since we don't want to link
-  # to `libboost_atomic-mt.dylib` within `tag.framework`. 
-  if(NOT BUILD_FRAMEWORK)
-    find_package(Boost COMPONENTS atomic)
-    if(Boost_ATOMIC_FOUND)
-      set(HAVE_BOOST_ATOMIC 1)
-    else()
-      set(HAVE_BOOST_ATOMIC 0)
-    endif()
-  endif()
+  check_cxx_source_compiles("
+    int main() {
+      volatile int x;
+      __sync_add_and_fetch(&x, 1);
+      int y = __sync_sub_and_fetch(&x, 1);
+      return 0;
+    }
+  " HAVE_GCC_ATOMIC)
 
-  if(NOT HAVE_BOOST_ATOMIC)
+  if(NOT HAVE_GCC_ATOMIC)
     check_cxx_source_compiles("
+      #include <libkern/OSAtomic.h>
       int main() {
-        volatile int x;
-        __sync_add_and_fetch(&x, 1);
-        int y = __sync_sub_and_fetch(&x, 1);
+        volatile int32_t x;
+        OSAtomicIncrement32Barrier(&x);
+        int32_t y = OSAtomicDecrement32Barrier(&x);
         return 0;
       }
-    " HAVE_GCC_ATOMIC)
+    " HAVE_MAC_ATOMIC)
 
-    if(NOT HAVE_GCC_ATOMIC)
+    if(NOT HAVE_MAC_ATOMIC)
       check_cxx_source_compiles("
-        #include <libkern/OSAtomic.h>
+        #include <windows.h>
         int main() {
-          volatile int32_t x;
-          OSAtomicIncrement32Barrier(&x);
-          int32_t y = OSAtomicDecrement32Barrier(&x);
+          volatile LONG x;
+          InterlockedIncrement(&x);
+          LONG y = InterlockedDecrement(&x);
           return 0;
         }
-      " HAVE_MAC_ATOMIC)
+      " HAVE_WIN_ATOMIC)
 
-      if(NOT HAVE_MAC_ATOMIC)
+      if(NOT HAVE_WIN_ATOMIC)
         check_cxx_source_compiles("
-          #include <windows.h>
+          #include <ia64intrin.h>
           int main() {
-            volatile LONG x;
-            InterlockedIncrement(&x);
-            LONG y = InterlockedDecrement(&x);
+            volatile int x;
+            __sync_add_and_fetch(&x, 1);
+            int y = __sync_sub_and_fetch(&x, 1);
             return 0;
           }
-        " HAVE_WIN_ATOMIC)
-
-        if(NOT HAVE_WIN_ATOMIC)
-          check_cxx_source_compiles("
-            #include <ia64intrin.h>
-            int main() {
-              volatile int x;
-              __sync_add_and_fetch(&x, 1);
-              int y = __sync_sub_and_fetch(&x, 1);
-              return 0;
-            }
-          " HAVE_IA64_ATOMIC)
-        endif()
+        " HAVE_IA64_ATOMIC)
       endif()
     endif()
   endif()
@@ -230,15 +216,6 @@ if(NOT ZLIB_SOURCE)
   else()
     set(HAVE_ZLIB 0)
   endif()
-
-  if(NOT HAVE_ZLIB)
-    find_package(Boost COMPONENTS iostreams zlib)
-    if(Boost_IOSTREAMS_FOUND AND Boost_ZLIB_FOUND)
-      set(HAVE_BOOST_ZLIB 1)
-    else()
-      set(HAVE_BOOST_ZLIB 0)
-    endif()
-  endif()
 endif()
 
 # Determine whether CppUnit is installed.
diff --git a/config.h.cmake b/config.h.cmake
index 90ae2eb..fd71613 100644
--- a/config.h.cmake
+++ b/config.h.cmake
@@ -13,7 +13,6 @@
 
 /* Defined if your compiler supports some atomic operations */
 #cmakedefine   HAVE_STD_ATOMIC 1
-#cmakedefine   HAVE_BOOST_ATOMIC 1
 #cmakedefine   HAVE_GCC_ATOMIC 1
 #cmakedefine   HAVE_MAC_ATOMIC 1
 #cmakedefine   HAVE_WIN_ATOMIC 1
@@ -28,7 +27,6 @@
 
 /* Defined if zlib is installed */
 #cmakedefine   HAVE_ZLIB 1
-#cmakedefine   HAVE_BOOST_ZLIB 1
 
 /* Indicates whether debug messages are shown even in release mode */
 #cmakedefine   TRACE_IN_RELEASE 1
diff --git a/taglib/CMakeLists.txt b/taglib/CMakeLists.txt
index 70ff8d1..4ea5430 100644
--- a/taglib/CMakeLists.txt
+++ b/taglib/CMakeLists.txt
@@ -32,7 +32,7 @@ elseif(HAVE_ZLIB_SOURCE)
   include_directories(${ZLIB_SOURCE})
 endif()
 
-if(HAVE_BOOST_BYTESWAP OR HAVE_BOOST_ATOMIC OR HAVE_BOOST_ZLIB)
+if(Boost_FOUND)
   include_directories(${Boost_INCLUDE_DIR})
 endif()
 
@@ -344,18 +344,10 @@ set(tag_LIB_SRCS
 
 add_library(tag ${tag_LIB_SRCS} ${tag_HDRS})
 
-if(ZLIB_FOUND)
+if(HAVE_ZLIB AND NOT HAVE_ZLIB_SOURCE)
   target_link_libraries(tag ${ZLIB_LIBRARIES})
 endif()
 
-if(HAVE_BOOST_ATOMIC)
-  target_link_libraries(tag ${Boost_ATOMIC_LIBRARY})
-endif()
-
-if(HAVE_BOOST_ZLIB)
-  target_link_libraries(tag ${Boost_IOSTREAMS_LIBRARY} ${Boost_ZLIB_LIBRARY})
-endif()
-
 set_target_properties(tag PROPERTIES
   VERSION ${TAGLIB_SOVERSION_MAJOR}.${TAGLIB_SOVERSION_MINOR}.${TAGLIB_SOVERSION_PATCH}
   SOVERSION ${TAGLIB_SOVERSION_MAJOR}
diff --git a/taglib/toolkit/trefcounter.cpp b/taglib/toolkit/trefcounter.cpp
index 27d17b8..eb2aa69 100644
--- a/taglib/toolkit/trefcounter.cpp
+++ b/taglib/toolkit/trefcounter.cpp
@@ -34,11 +34,6 @@
 # define ATOMIC_INT std::atomic<unsigned int>
 # define ATOMIC_INC(x) x.fetch_add(1)
 # define ATOMIC_DEC(x) (x.fetch_sub(1) - 1)
-#elif defined(HAVE_BOOST_ATOMIC)
-# include <boost/atomic.hpp>
-# define ATOMIC_INT boost::atomic<unsigned int>
-# define ATOMIC_INC(x) x.fetch_add(1)
-# define ATOMIC_DEC(x) (x.fetch_sub(1) - 1)
 #elif defined(HAVE_GCC_ATOMIC)
 # define ATOMIC_INT int
 # define ATOMIC_INC(x) __sync_add_and_fetch(&x, 1)
diff --git a/taglib/toolkit/tzlib.cpp b/taglib/toolkit/tzlib.cpp
index 40158fd..6d07ba3 100644
--- a/taglib/toolkit/tzlib.cpp
+++ b/taglib/toolkit/tzlib.cpp
@@ -27,23 +27,19 @@
 # include <config.h>
 #endif
 
-#if defined(HAVE_ZLIB)
+#ifdef HAVE_ZLIB
 # include <zlib.h>
-#elif defined(HAVE_BOOST_ZLIB)
-# include <boost/iostreams/filtering_streambuf.hpp>
-# include <boost/iostreams/filter/zlib.hpp>
+# include <tstring.h>
+# include <tdebug.h>
 #endif
 
-#include <tstring.h>
-#include <tdebug.h>
-
 #include "tzlib.h"
 
 using namespace TagLib;
 
 bool zlib::isAvailable()
 {
-#if defined(HAVE_ZLIB) || defined(HAVE_BOOST_ZLIB)
+#ifdef HAVE_ZLIB
 
   return true;
 
@@ -56,7 +52,7 @@ bool zlib::isAvailable()
 
 ByteVector zlib::decompress(const ByteVector &data)
 {
-#if defined(HAVE_ZLIB)
+#ifdef HAVE_ZLIB
 
   z_stream stream = {};
 
@@ -102,38 +98,6 @@ ByteVector zlib::decompress(const ByteVector &data)
 
   return outData;
 
-#elif defined(HAVE_BOOST_ZLIB)
-
-  using namespace boost::iostreams;
-
-  struct : public sink
-  {
-    ByteVector data;
-
-    typedef char     char_type;
-    typedef sink_tag category;
-
-    std::streamsize write(char const* s, std::streamsize n)
-    {
-      const unsigned int originalSize = data.size();
-
-      data.resize(static_cast<unsigned int>(originalSize + n));
-      ::memcpy(data.data() + originalSize, s, static_cast<size_t>(n));
-
-      return n;
-    }
-  } sink;
-
-  try {
-    zlib_decompressor().write(sink, data.data(), data.size());
-  }
-  catch(const zlib_error &) {
-    debug("zlib::decompress() - Error reading compressed stream.");
-    return ByteVector();
-  }
-
-  return sink.data;
-
 #else
 
   return ByteVector();
